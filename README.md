 # product-store-service

I didn't get much time as we are doing many emergency prod implemtation for the corono virus alerts. Just tried to show my understanding of spring

## Issues
 - Did not create seperate response for the event with only product ID. It will return full deatils. 
 

## Tech Stack
 - Java 8
 - Spring Boot

## Tools
 - Gradle for build
 - jUnit for testing

## Steps to run [From project root folder or any IDE]
Build : [same added in setup file]
```sh
$ gradlew clean build
```
Run: 
Stand alone jar
```sh
$ java -jar build/libs/product-store-service-0.0.1-SNAPSHOT.jar
```

Test:
* Can access Swagger from this url
  ``` http://localhost:8087/swagger-ui.html ```
* We can use APIs directly from url ``` http://localhost:8087/event/```
* POST,PUT, GET and GET ALL  Methods are available

Import into IDE:
Should imported as gradle project in any IDE

we can run from IDE using ``` ProductStoreServiceApplication.java ``` class file

## Design
- Spring repository for Database queries
- Transformers are meant for convert POJO from view to entity and reverse
 
 - Database tables:
   * Product (Master)
   * Event 
   * Event_product 
 
## Improvements:
  - Security implementation
  - More flexibility to extend the apis
  - Pagenation for get apis
  
 

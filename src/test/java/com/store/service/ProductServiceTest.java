package com.store.service;

import com.store.entity.Product;
import com.store.repository.ProductRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ProductServiceTest {

    @Autowired
    private ProductRepository mockProductRepository;

    @Autowired
    private ProductService productServiceUnderTest;

    @BeforeEach
    void setUp() {
        final Product product = new Product(1L, "name");
        final Product result = productServiceUnderTest.save(product);
    }

    @Test
    void givenValidInputThenSaveSuccess() {
        final Product product = new Product(1L, "name");
        final Product expectedResult = new Product(1L, "name");
        final Product result = productServiceUnderTest.save(product);
        assertEquals(expectedResult, result);
    }

    @Test
    void givenInvalidInputThenSaveFailed() {
        final Product product = new Product();
        Exception exception = assertThrows(Exception.class, () -> productServiceUnderTest.save(product));
        assertNotNull(exception);
    }

    @Test
    void givenValidProductThenExist() {
        final Product product = new Product(1L, "name");
        final Boolean result = productServiceUnderTest.isExist(product);
        assertTrue(result);
    }

    @Test
    void givenInvalidProductThenFail() {
        final Product product = new Product(0L, "name");
        final Boolean result = productServiceUnderTest.isExist(product);
        assertFalse(result);
    }
}

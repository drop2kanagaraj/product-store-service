package com.store.service;

import com.store.entity.Event;
import com.store.model.EventModel;
import com.store.model.EventProductModel;
import com.store.repository.EventProductRepository;
import com.store.repository.EventRepository;
import com.store.transformer.entity.EventEntityTransformer;
import com.store.transformer.entity.EventProductEntityTransformer;
import com.store.transformer.model.EventModelTransformer;
import com.store.transformer.model.EventProductModelTransformer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@SpringBootTest
class EventStoreServiceTest {

    @Autowired
    private EventRepository eventRepository;
    @Autowired
    private ProductService productService;
    @Autowired
    private EventProductRepository eventProductRepository;

    @Autowired
    private EventModelTransformer eventModelTransformer;
    @Autowired
    private EventEntityTransformer eventEntityTransformer;

    @Autowired
    private EventStoreService eventStoreServiceUnderTest;

    @Autowired
    private EventProductModelTransformer eventProductModelTransformer;

    @Autowired
    private EventProductEntityTransformer eventProductEntityTransformer;

    @BeforeEach
    void setUp() {
        eventStoreServiceUnderTest.save(getEventModel());
    }

    @Test
    void testSave() {
        final EventModel eventModel = getEventModel();
        final EventModel result = eventStoreServiceUnderTest.save(eventModel);
        assertNotNull(result);
    }

    @Test
    void testGetAllEvents() {
        final List<EventModel> result = eventStoreServiceUnderTest.getAllEvents();
        assertTrue(!result.isEmpty());
    }

    @Test
    void testGetByEventId() {
        final List<EventModel> result = eventStoreServiceUnderTest.getAllEvents();
        final Optional<EventModel> event = eventStoreServiceUnderTest.getByEventId(result.get(0).getId());
        assertTrue(event.isPresent());
    }

    @Test
    void testIsExits() {
        final List<EventModel> result = eventStoreServiceUnderTest.getAllEvents();
        final boolean exits = eventStoreServiceUnderTest.isExits(result.get(0).getId());
        assertTrue(exits);
    }

    private EventModel getEventModel() {
        final EventModel eventModel = new EventModel();
        eventModel.setTimestamp(LocalDateTime.now());
        eventModel.setId("test-event");
        EventProductModel eventProductModel = new EventProductModel();
        eventProductModel.setName("test");
        eventProductModel.setQuantity(10);
        eventProductModel.setSaleAmount(BigDecimal.valueOf(10000));
        eventModel.setEventProducts(Arrays.asList(eventProductModel));
        return eventModel;
    }
}

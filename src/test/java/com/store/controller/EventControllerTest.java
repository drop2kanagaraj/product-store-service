package com.store.controller;

import com.store.model.EventModel;
import com.store.model.EventProductModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class EventControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @BeforeEach
    public void setup() throws MalformedURLException {
        EventModel eventModel = getEventModel();
        ResponseEntity<EventModel> eventModelResponseEntity = restTemplate.postForEntity(new URL(String.format("http://localhost:%d/event", port)).toString(),
                eventModel,
                EventModel.class);
    }

    @Test
    public void post() throws MalformedURLException {
        EventModel eventModel = getEventModel();
        ResponseEntity<EventModel> eventModelResponseEntity = restTemplate.postForEntity(new URL(String.format("http://localhost:%d/event", port)).toString(),
                eventModel,
                EventModel.class);
        assertTrue(eventModelResponseEntity.getStatusCode() == HttpStatus.OK);
    }

    @Test
    public void put() throws MalformedURLException {
        EventModel eventModel = getEventModel();
         restTemplate.put(new URL(String.format("http://localhost:%d/event/" + eventModel.getId(), port)).toString(),
                eventModel,
                EventModel.class);

        ResponseEntity<EventModel> restTemplateForEntity = restTemplate.getForEntity(new URL(String.format("http://localhost:%d/event/" + eventModel.getId(), port)).toString(), EventModel.class);
        assertNotNull(restTemplateForEntity);
    }

    private EventModel getEventModel() {
        final EventModel eventModel = new EventModel();
        eventModel.setId("test-event");
        eventModel.setTimestamp(null);
        EventProductModel eventProductModel = new EventProductModel();
        eventProductModel.setName("test");
        eventProductModel.setQuantity(10);
        eventProductModel.setSaleAmount(BigDecimal.valueOf(10000));
        eventModel.setEventProducts(Arrays.asList(eventProductModel));
        return eventModel;
    }

}
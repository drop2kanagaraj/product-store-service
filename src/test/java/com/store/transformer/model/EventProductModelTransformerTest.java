package com.store.transformer.model;

import com.store.entity.EventProduct;
import com.store.model.EventProductModel;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class EventProductModelTransformerTest {

    @Autowired
    private EventProductModelTransformer eventProductModelTransformerUnderTest;

    @Test
    void testApply() {
        final EventProductModel eventProductModel = new EventProductModel();
        eventProductModel.setName("event");
        eventProductModel.setQuantity(10);
        final EventProduct result = eventProductModelTransformerUnderTest.apply(eventProductModel);
        assertEquals(10, result.getQuantity());
    }
}

package com.store.transformer.model;

import com.store.entity.Event;
import com.store.model.EventModel;
import com.store.model.EventProductModel;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class EventModelTransformerTest {

    @Autowired
    private EventModelTransformer eventModelTransformerUnderTest;

    @Test
    void testApply() {
        final EventModel eventModel = new EventModel();
        eventModel.setId("event");
        eventModel.setEventProducts(Arrays.asList(new EventProductModel()));
        final Event result = eventModelTransformerUnderTest.apply(eventModel);
        assertEquals("event", result.getId());
    }
}

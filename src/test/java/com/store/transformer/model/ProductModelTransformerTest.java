package com.store.transformer.model;

import com.store.entity.Product;
import com.store.model.ProductModel;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class ProductModelTransformerTest {

    @Autowired
    private ProductModelTransformer productModelTransformerUnderTest;

    @Test
    void testApply() {
        final ProductModel productModel = new ProductModel();
        productModel.setId(1L);
        productModel.setName("name");
        final Product result = productModelTransformerUnderTest.apply(productModel);
        assertEquals("name", result.getName());
    }
}

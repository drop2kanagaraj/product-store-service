package com.store.transformer.entity;

import com.store.entity.Product;
import com.store.model.ProductModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class ProductEntityTransformerTest {

    @Autowired
    private ProductEntityTransformer productEntityTransformerUnderTest;

    @Test
    void testApply() {
        final Product product = new Product(0L, "name");
        final ProductModel result = productEntityTransformerUnderTest.apply(product);
        assertEquals("name", result.getName());
    }
}

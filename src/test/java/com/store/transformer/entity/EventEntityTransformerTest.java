package com.store.transformer.entity;

import com.store.entity.Event;
import com.store.model.EventModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class EventEntityTransformerTest {

    @Autowired
    private EventEntityTransformer eventEntityTransformerUnderTest;

    @BeforeEach
    void setUp() {
    }

    @Test
    void testApply() {
        final Event event = new Event();
        final EventModel expectedResult = new EventModel();
        final EventModel result = eventEntityTransformerUnderTest.apply(event);
        assertEquals(expectedResult, result);
    }
}

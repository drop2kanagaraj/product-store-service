package com.store.transformer.entity;

import com.store.entity.EventProduct;
import com.store.entity.Product;
import com.store.model.EventProductModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class EventProductEntityTransformerTest {

    @Autowired
    private EventProductEntityTransformer eventProductEntityTransformerUnderTest;

    @Test
    void testApply() {
        final EventProduct eventProduct = new EventProduct();
        eventProduct.setQuantity(10);
        eventProduct.setProduct(new Product(1L, "Name"));
        final EventProductModel result = eventProductEntityTransformerUnderTest.apply(eventProduct);
        assertEquals(10, result.getQuantity());
    }
}

package com.store.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@Entity(name = "event")
public class Event {

    @Id
    private String id;

    private LocalDateTime timestamp;

    @PrePersist
    private void generateId() {
        this.setId(UUID.randomUUID().toString());
    }

    @OneToMany(mappedBy = "event", fetch = FetchType.EAGER)
    @JsonManagedReference
    private List<EventProduct> eventProducts;
}

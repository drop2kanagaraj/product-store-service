package com.store.transformer.entity;

import com.store.entity.Product;
import com.store.model.ProductModel;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
public class ProductEntityTransformer {

    public ProductModel apply(Product product) {
        ProductModel productModel = new ProductModel();
        BeanUtils.copyProperties(product, productModel);
        return productModel;
    }

}

package com.store.transformer.entity;

import com.store.entity.EventProduct;
import com.store.entity.Product;
import com.store.model.EventProductModel;
import com.store.model.ProductModel;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
public class EventProductEntityTransformer {


    public EventProductModel apply(EventProduct eventProduct) {
        EventProductModel eventProductModel = new EventProductModel();
        BeanUtils.copyProperties(eventProduct, eventProductModel);
        eventProductModel.setName(eventProduct.getProduct().getName());
        return eventProductModel;
    }

}

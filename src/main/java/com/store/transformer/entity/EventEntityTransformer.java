package com.store.transformer.entity;

import com.store.entity.Event;
import com.store.entity.EventProduct;
import com.store.model.EventModel;
import com.store.repository.EventProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class EventEntityTransformer {

    private final EventProductRepository eventProductRepository;
    private final EventProductEntityTransformer eventProductEntityTransformer;

    public EventModel apply(Event event) {
        EventModel eventModel = new EventModel();
        BeanUtils.copyProperties(event, eventModel);
        Optional<List<EventProduct>> eventProducts = eventProductRepository.findByEventId(event.getId());
        if (eventProducts.isPresent()) {
            eventModel.setEventProducts(eventProducts.get().stream().map(eventProductEntityTransformer::apply).collect(Collectors.toList()));
        }
        return eventModel;
    }

}

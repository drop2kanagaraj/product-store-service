package com.store.transformer.model;

import com.store.entity.Product;
import com.store.model.ProductModel;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
public class ProductModelTransformer {

    public Product apply(ProductModel productModel) {
        Product product = new Product();
        BeanUtils.copyProperties(productModel, product);
        return product;
    }

}

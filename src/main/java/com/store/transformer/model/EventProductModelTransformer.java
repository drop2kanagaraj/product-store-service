package com.store.transformer.model;

import com.store.entity.EventProduct;
import com.store.entity.Product;
import com.store.model.EventProductModel;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
public class EventProductModelTransformer {

    public EventProduct apply(EventProductModel eventProductModel) {
        EventProduct eventProduct = new EventProduct();
        eventProduct.setQuantity(eventProductModel.getQuantity());
        eventProduct.setSaleAmount(eventProductModel.getSaleAmount());
        eventProduct.setProduct(new Product(eventProductModel.getId(), eventProductModel.getName()));
        return eventProduct;
    }

}

package com.store.transformer.model;

import com.store.entity.Event;
import com.store.entity.EventProduct;
import com.store.model.EventModel;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class EventModelTransformer {

    private final EventProductModelTransformer eventProductModelTransformer;

    public Event apply(EventModel eventModel) {
        Event event = new Event();
        BeanUtils.copyProperties(eventModel, event);
        if (eventModel.getTimestamp() == null) {
            event.setTimestamp(LocalDateTime.now());
        }
        List<EventProduct> products = eventModel.getEventProducts()
                .stream()
                .map(eventProductModelTransformer::apply)
                .collect(Collectors.toList());
        event.setEventProducts(products);
        return event;
    }

}

package com.store.repository;


import com.store.entity.EventProduct;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface EventProductRepository extends JpaRepository<EventProduct, Long> {

    public Optional<List<EventProduct>> findByEventId(String id);
}

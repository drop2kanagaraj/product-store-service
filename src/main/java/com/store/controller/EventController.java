package com.store.controller;

import com.store.model.EventModel;
import com.store.service.EventStoreService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/event")
@AllArgsConstructor
@Validated
public class EventController {

    private final EventStoreService eventStoreService;

    @PostMapping
    public ResponseEntity<EventModel> post(@RequestBody @Valid EventModel eventModel) {
        return ResponseEntity.ok(eventStoreService.save(eventModel));
    }

    @PutMapping("/{eventId}")
    public ResponseEntity<EventModel> put(@PathVariable String eventId, @RequestBody @Valid EventModel eventModel) {
        if (eventStoreService.isExits(eventId)) {
            return ResponseEntity.ok(eventStoreService.save(eventModel));
        }
        return ResponseEntity.noContent().build();
    }

    @GetMapping
    public ResponseEntity<List<EventModel>> get() {
        return ResponseEntity.ok(eventStoreService.getAllEvents());
    }

    @GetMapping("/{eventId}")
    public ResponseEntity<EventModel> getByEvent(@PathVariable String eventId) {
        Optional<EventModel> event = eventStoreService.getByEventId(eventId);
        if (event.isPresent()) {
            return ResponseEntity.ok(event.get());
        }
        return ResponseEntity.noContent().build();
    }
}

package com.store.service;

import com.store.entity.Product;
import com.store.repository.ProductRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;

    public Product save(Product product) {
        return productRepository.save(product);
    }

    public Boolean isExist(Product product){
        return productRepository.findById(product.getId()).isPresent();
    }
}

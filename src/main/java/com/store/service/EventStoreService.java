package com.store.service;

import com.store.entity.Event;
import com.store.entity.EventProduct;
import com.store.model.EventModel;
import com.store.repository.EventProductRepository;
import com.store.repository.EventRepository;
import com.store.transformer.entity.EventEntityTransformer;
import com.store.transformer.model.EventModelTransformer;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class EventStoreService {

    private final EventRepository eventRepository;
    private final EventModelTransformer eventModelTransformer;
    private final EventEntityTransformer eventEntityTransformer;
    private final ProductService productService;
    private final EventProductRepository eventProductRepository;

    public EventModel save(EventModel eventModel) {
        Event event = eventModelTransformer.apply(eventModel);
        Event eventEntity = eventRepository.save(eventModelTransformer.apply(eventModel));
        for (EventProduct eventProduct : event.getEventProducts()) {
            productService.save(eventProduct.getProduct());
            eventProduct.setEvent(eventEntity);
            eventProductRepository.save(eventProduct);
        }
        return getByEventId(eventEntity.getId()).get();
    }

    public List<EventModel> getAllEvents() {
        List<Event> events = eventRepository.findAll();
        return events.stream().map(eventEntityTransformer::apply).collect(Collectors.toList());
    }

    public Optional<EventModel> getByEventId(String id) {
        Optional<Event> event = eventRepository.findById(id);
        if (event.isPresent()) {
            EventModel eventModel = event.map(eventEntityTransformer::apply).get();
            return Optional.of(eventModel);
        }
        return Optional.empty();
    }

    public boolean isExits(String id) {
        return getByEventId(id).isPresent();
    }
}

package com.store.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
@ApiModel
@Valid
public class EventModel {

    @JsonProperty
    private String id;

    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(dataType = "java.lang.String", example = "30/01/2020 00:00:00")
    private LocalDateTime timestamp;

    @JsonProperty(required = true, value = "products")
    @Size(min = 1, message = "Product list cannot be null!")
    private List<EventProductModel> eventProducts;

}

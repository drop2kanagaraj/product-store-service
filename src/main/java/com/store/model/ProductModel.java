package com.store.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.Valid;

@Data
@ApiModel
@JsonIgnoreProperties(ignoreUnknown = true)
@Valid
public class ProductModel {

    @JsonProperty
    private Long id;

    @JsonProperty
    @JsonIgnore
    private String name;

}

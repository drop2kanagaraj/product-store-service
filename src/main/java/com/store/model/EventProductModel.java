package com.store.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.validation.Valid;
import java.math.BigDecimal;

@Data
@ApiModel
@JsonIgnoreProperties(ignoreUnknown = true)
@Valid
public class EventProductModel {

    @JsonProperty
    private Long id;

    @JsonProperty(required = true)
    private String name;

    @JsonProperty(required = true)
    private Integer quantity;

    @JsonProperty(value = "sale_amount", required = true)
    private BigDecimal saleAmount;

}
